# Gremienbot
Discord-Python-Bot zur (Gremien-) Sitzungsverwaltung, welcher eine Redeliste und eine geheime Wahl bereit stellt.

---

## Motivation
Durch die Corona-Kriese war es notwendig digitale Sitzung zu ermöglichen. Zwar gibt es einige Videokonferenztools, 
allerding boten diese nicht den gleichen Funktionsumfang für die gwohnte Sitzungen an. Es fehlte eine passiv verwaltbare 
Redeliste und die Mögichkeit einer geheimen Wahl, ohne eine externe Software/Website zu nutzen.
Insgesamt wurde daher Discord ausgesucht, da es dort die Möglichkeit der Bots gab.

---

## Build status
Bot ist vollständig nutzbar und dokumentiert. Kleinere Bugs sind noch vorhanden, aber nicht vollständig identifiziert.
Code folgt noch keinem Standard, da erst quick and dirty implementiert. 

- Nächste Features - Code:
    - Sauberere Implementation/ Neustrukturierung mit der Option für mehrere Wahlen und Redelisten
    - Offene Abstimmungen
    - Gestaffelte geheime Abstimmung
    
- Nächste Features - Dokumentation:
    - Discord Bot mit Server verknüpfen
    - Installations Guide: heroku
    - Windows


---

## Installation
**Kurz**
- Notwendige Bestandteile:
    - Python 3.X, X >= 5
    - pip
    - virtualenv
    - discord.py
    - User rolls in Discord: "Administrator" und "Redeleitung"
    
- Sinnvolle Bestandteile:
    - Heroku (Dauerhaftes Hosting)
    - Git (Für heroku notw., Versionskontrolle ) 
  
 
 **Lang**
 
 Erstmal nur via Linux Ubuntu (und Ablegern)
 - Python:
    - Meist vorinstalliert. Prüfe via console: `python3`
        - falls error: Installation via apt: 
            - `sudo apt update` 
            - `sudo add-apt-repository ppa:deadsnakes/ppa`
            - `sudo apt update`
            - `sudo apt install python3.X python3.X-dev`, wobei X die jeweilige Version ist
- pip:
    - `wget https://bootstrap.pypa.io/get-pip.py`
    - `sudo python3 get-pip.py`
    - `sudo rm -rf ~/get-pip.py ~/.cache/pip`
    
- virtualenv:
    - `sudo pip install virtualenv virtualenvwrapper`
    - edit in `~/.bashrc`:
        - export WORKON_HOME=$HOME/.virtualenvs
        - export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.X **(!)**
        - source /usr/local/bin/virtualenvwrapper.sh
    - `source ~/.bashrc`
    - `mkvirtualenv NAME -p python3` - überprüfe welches python genutzt werden soll
- `workon NAME`
    - `pip3 install discord`
        

- Discord-Bot verknüpfen
    - discord.com , Konto benötigt, anmelden
    - im Reiter "Entwickler" -> "Entwicklerportal"
    - Klick auf "New Application" (oben rechts)
        - Eindeutiger, sinnvoller Name
    - Klick auf erstellten Bot
        - Klick auf "Bot"-Reiter -> "Add Bot" --> Bestätigen
        - Klick auf "OAut2":
            - "bot" markieren
            - Permissions "Administrator" markieren
            - entstehenden Link kopieren und in neuen Browsertab einfügen
        - Anweisungen folgen
        - Zurück zum Entwicklerportal
        - Klick auf "Bot", kopieren des Tokens und einfügen in Code - Letzte Zeile 
        

## How to use
**Allgemeines**
- Die Person(en), die_der die Redeleitung übernehmen sollen, müssen als Rolle zusätzlch "Redeleitung" zugewiesen bekommen. 
- Bei Wahl:
    - bei der geheimen Wahl können eine oder mehrere Personen gewählt werden. 
    "Nein" und "Enthaltung" müssen nicht angegeben werden.
        - Eine Person: 
        `$wahl open normal max/mustermann/maxmustermann`
            - Es können mehr oder weniger Varianten genutzt werden
            - Leerzeichen können nicht verwendet werden
        - mehrere Personen:
        `$wahl open normal max/mustermann/maxmustermann;nele/musterfrau/nelemusterfrau/datnele`
    - Der jeweilige Chat muss erst initiaöisiert werden. Momentan kann nur ein Chat für die Redeliste und Wahl genutzt werden.
    Dies muss nicht der gleiceh sein
    - Der Name des Bots muss einmal im Code im Abschnitt con `$clean` hinzugeügt werden ~ Zeile 550

**Befehle**
- Redeliste:
    - Für Teilnehmer_in: 
        - `$redeliste normal` - Setzt den_die Teilnehmer_in auf die normale Redeliste
        - `$redeliste direkt` - Setzt den_die Teilnehmer_in auf die Redeliste, die dirket zum Aktuellen Thema gehört
        - `$redeliste go` - Hebt den Wunsch auf einen GO-Antrag hervor
        
    - Für Redeleitung:
        - `$init_liste` - Initialisiert den Text Chat, in dem die Redeliste verwendet werden soll
        - `$redeliste open` - (Wiederer-) Öffnet die Redeliste
        - `$redeliste close` - Schließt die Redeliste
        - `$redeliste next` - Nächte_r Redner_in wird bekanntgegeben
        - `$redeliste show X` - X = all / normal / direkt ; zeigt die entsprechenden Redelisten
        - `$redeliste delete X` - X = all / normal / direkt ; löscht die entsprechenden Redelisten   
    
- Wahl
    - Für Teilnehmer_in:
        - Direktnachricht an Bot:
            - `$wahl anmeldung` - Meldet den_die Teilnehmer_in zur geheimen Wahl an
            - `$wahl geheim normal X` - X = Name / Nein / Enthaltung
    
    - Für Redeleitung:
        - `$wahl initialise` - Initialisiert den gewünschten Text-Chat Channel
        - `$wahl open normal name1.1/name1.2/...;name2.1/name2.2/...;... ` 
            - Öffnet die Wahl mit den entsprechnenden Kandidierenden und Namensvariationen und gibt die Vorschläge aus
        - `$wahl close` - Schließt die Wahl und gibt das Ergebnis aus
       
- Clean
    - Nur für Redeleitung
        - `$clean botmsg` - Löscht alle Bot-Nachrichten - **Name muss im Code eingefügt werden**
        - `$clean fullclean` - Löscht alle Nachrichten im Textchat
            
     

             
 
 ---
    
## Contribution
Pull requests sind gerne gesehen.



    


