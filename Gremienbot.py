import discord
import time


class MyClient(discord.Client):
    def __init__(self, **options):
        # 'Globale' Variablen werden hier deklariert
        super().__init__(**options)
        self.redeListe = [] # Liste, welche alle Personen auf der normalen Redeliste enhält
        self.direktDazu = [] # Liste, welche Personen enhält, welche etwas zum aktuellen Redebeitrag etwas sagen wollen
        self.currentSpeaker = None # Veraltete Struktur, welche die aktuell sprechende Person enthält
        self.currentDirectSpeaker = None # Veraltete Struktur, welche die Person enthält, die zuletzt ein 'Direkt-dazu' hatte
        self.redeListeOffen = True # Variable, welche anzeigt, ob die Redeliste offen ist
        self.wahlRegister = [] # Liste, die alle aktuell registrierten Wähler*innen enthält
        self.wahlurne = {} # Liste, welche die aktuellen Stimmzettel enthält
        self.wahlAuswahl = {} # Liste, welche alle zu wählenden Personen enthält
        self.wahlOffen = False # Variable, die anzeigt, ob die Wahl geöffnet ist
        self.anzahlWahlOptionen = 0 # Variable, die anzeigt, wie viele Personen/optionen zur Wahl stehen
        self.stimmzettelInUrne = [] # Variable, die anzeigt, wie viele Stimmzettel in der Urne sind
        self.fehlerhafteStimmen = [] # Liste, die alle fehlerhaften/ungültigen Stimmen enthält
        self.strictSpeechList = False # (momentan nicht genutzte)Variable, die anzeigt, ob eine strikte Redeliste vorherrscht
        self.wahlID = None # Variable, die die ID des Chats enthält, in welchem die Wahl angezeigt wird
        self.redelisteID = None # Variable, die die ID des Chats enhält, in dem die Redeliste geführt wird

    ''' Funkion die aufgerufen wird, sobald irgendeine Nachricht auf dem Server geschickt wird. '''

    async def on_message(self, massage):
        # Anfrage wird verworfen, falls die Nachricht vom Bot kommt
        if massage.author == client.user:
            return

        # Abschnitt zum Steuern der geheimen Wahl
        if str(massage.content).startswith("$wahl"):
            # Funktionsaufruf besteht aus mehreren Segmenten, die mit einem Leerzeichen getennt werden
            wahlInfo = str(massage.content).split(" ")

            # Liste, die die Rollen des Nachrichtensendenden enhält
            userRoles = []
            try:
                for role in massage.author.roles:
                    userRoles.append(str(role.name).lower())
            except:
                pass

            # Falls Informationen fehlen, wird die Anfrage verworfen
            if len(wahlInfo) < 2:
                await massage.channel.send("Ich habe dich leider nicht verstanden.")
                return

            # Initialisiert den jeweiligen Channel für die geheime Wahl
            elif wahlInfo[1] == "initialise":
                if self.wahlID == None:
                    self.wahlID = massage.channel.id
                    await massage.channel.send("Die Wahl wurde initialisiert!")
                    return

                else:
                    self.wahlID = massage.channel.id
                    await massage.channel.send("Die Wahl wurde neu initialisiert!")
                    return

            # Öffnen der Wahl - bisher nur "normale" geheime Wahl - Übergabe der Kandidaten und der zugehörigen Auswahlmöglichkeiten
            elif (wahlInfo[1].lower() == "open") and ("redeleitung" in userRoles):
                if massage.channel.id != self.wahlID:
                    await massage.channel.send("Bitte eröffne die Wahl in der Gruppensitzung")
                    return

                # Falls nicht alle Informationen übergeben wurden, wird die Wahl verworfen
                elif len(wahlInfo) < 4:
                    await massage.channel.send("Für die Wahl fehlen noch Informationen!")
                    return

                # Falls alle notwendigen Parameter übergeben wirden, wird die Wahl geöffnet und die Optionen ausgegeben
                elif len(wahlInfo) == 4:
                    self.wahlOffen = True # Öffnen der Wahl

                    # die verschiedenen Optionen werden durch ein ';' getrennt und in 'optionen' gespeichert
                    optionen = wahlInfo[3].split(";")
                    self.anzahlWahlOptionen = len(optionen)

                    # Unterscheidung, ob es nur eine oder mehrere Option(en) gibt, über die Abgestimmt werden kann
                    if len(optionen) == 1:
                        # Die verschiedenen Varianten einer Option werde mit einem '/' getrennt
                        varianten = optionen[0].split("/")
                        # Alle Varianten werden klein geschrieben
                        for var in range(len(varianten)):
                            varianten[var] = varianten[var].lower()
                        varianten.append("ja") # Bei nur einer Option kann auch nur mit 'ja' abgestimmt werden
                        self.wahlAuswahl[varianten[0]] = varianten
                        self.wahlAuswahl["nein"] = ["nein"]
                        self.wahlAuswahl["enthaltung"] = ["enthaltung"]
                        self.wahlurne = {varianten[0]: [0, 0, 0]} #  [a,b,c] steht für a*'Ja', b*'Nein', c*'Enthaltung'

                    else:
                        # Für jede Option werden entsprechenden varianten durch '/' getrennt
                        for option in optionen:
                            varianten = option.split("/")
                            # Alle varianten werden klein geschrieben, um Fehler zu vermeiden
                            for var in range(len(varianten)):
                                varianten[var] = varianten[var].lower()
                            self.wahlAuswahl[varianten[0]] = varianten
                            self.wahlurne[varianten[0]] = [0]
                        self.wahlAuswahl["nein"] = ["nein"]
                        self.wahlAuswahl["enthaltung"] = ["enthaltung"]

                        self.wahlurne["nein"] = [0]
                        self.wahlurne["enthaltung"] = [0]

                    await massage.channel.send("----- \t -----")
                    await massage.channel.send("Die Wahl wurde soeben geöffnet, meldet euch bitte an!")
                    await massage.channel.send(
                        "\n Es stehen {} Kandidat_innen zur Auswahl!".format(len(self.wahlAuswahl)))

                    # Löscht alle Bot-Befehle, die mit '$wahl' anfangen
                    async for msg in massage.channel.history():
                        if str(msg.content).startswith("$wahl"):
                            await msg.delete()

                    # Ausgabe der Optionen mit jeweiligen Varianten
                    for option in self.wahlAuswahl:
                        await massage.channel.send(
                            "Zur Wahl steht {} mit folgenden Möglichekiten {}".format(option, self.wahlAuswahl[option]))

                    # Funktion stoppt hier
                    return

                # Falls Informationen fehlen oder Dinge falsch geschriebn wurden, tut der Bot nichts
                else:
                    await massage.channel.send("Ich habe dich nicht verstanden!")
                    return

            # --- Abschnitt zum Schließen der Redeliste ---
            # Zweites Statement bei $wahl muss 'close' sein, der*die jenige muss 'Redeleitung' und Wahl muss offen sein
            elif wahlInfo[1].lower() == "close" and "redeleitung" in userRoles and self.wahlOffen:
                # Prüft, ob die initialisierte ID, die Channel ID der Nachticht ist.
                if massage.channel.id == self.wahlID:
                    await massage.channel.send("----- \t -----")
                    await massage.channel.send(
                        "Gülte Stimmen: {} \t Ungültige stimmen{}".format(self.wahlurne, self.fehlerhafteStimmen))

                    # Wahl wird geschlossen und alle Listen geleert
                    self.wahlOffen = False
                    self.wahlRegister = []
                    self.wahlurne = {}
                    self.wahlAuswahl = {}
                    self.stimmzettelInUrne = []
                    self.anzahlWahlOptionen = 0
                    self.fehlerhafteStimmen = []

                    # Alle Bot-Befehle mit '$wahl' werden gelöscht
                    async for msg in massage.channel.history():
                        if str(msg.content).lower().startswith("$wahl"):
                            await msg.delete()

                    await massage.channel.send("Die Wahl wurde soeben geschlossen!")
                    await massage.channel.send("----- \t -----")

                    return

                else:
                    return

            # --- Abschnit zum Anmelden zur Wahl ---
            # Abschnitt inzwischen tendentiell redundant, da prinzipiell auch direkt bei der Wahl einzupflegen
            elif wahlInfo[1].lower() == "anmeldung":

                # Ausgabe, falls Wahl nicht geöffnet sein sollte
                if not self.wahlOffen:
                    await massage.channel.send("Die Wahl ist leider noch nicht offen. Falls sie offen sein sollte, "
                                               "wende dich bitte an die Redeleitung!")
                    return

                # Setzte die ID des Nachrichten Channels, als Wahl-ID
                currentChannelID = massage.channel.id

                # Prüft, die Nachricht aus dem SItzungschannel stammt, falls ja, Abbruch
                if currentChannelID == self.wahlID:

                    await massage.channel.send(
                        "{}, bitte schreibe mir für die Anmeldung und die Wahl eine Direktnachricht,"
                        " um eine geheime Wahl gewährleisten zu können.".format(str(massage.author.name).split("#")[0]))

                    # Löscht alle Bot-Nachrichten mit '$wahl'
                    async for msg in massage.channel.history():
                        if str(msg.content).startswith("$wahl"):
                            await msg.delete()

                    return

                # User ID wird ins Wahlregister übernommen
                else:
                    self.wahlRegister.append(massage.channel.id)
                    await massage.channel.send("Du bist jetzt für die Wahlen angemeldet.")
                    return

            # --- Abschnitt zur Abstimmung ---
            elif wahlInfo[1].lower() == "geheim" and self.wahlOffen:
                currentChannelID = massage.channel.id

                # Prüft, ob die nachricht aus dem Sitzungchannel kommt
                if currentChannelID == self.wahlID:

                    await massage.channel.send(
                        "{}, bitte schreibe mir für die Anmeldung und die Wahl eine Direktnachricht,"
                        " um eine geheime Wahl gewährleisten zu können.".format(str(massage.author.name).split("#")[0]))

                    # Löscht alle Bot-Nachrichten, die mit '$wahl' beginnen
                    async for msg in massage.channel.history():
                        if str(msg.content).startswith("$wahl"):
                            await msg.delete()

                    return

                # Prüft, ob bereits abgestimmt wurde und weist ggf zurück
                elif currentChannelID in self.stimmzettelInUrne:
                    await massage.channel.send("Du hast bereits abgestimmt!")
                    return

                # Formal Korrekte Stimmenabgabe
                else:
                    # Fügt Abstimmenden zur Liste zu, die bereits abgestimmt haben
                    self.stimmzettelInUrne.append(currentChannelID)
                    # Falls nur eine Waloption zur Auswahl steht
                    if self.anzahlWahlOptionen == 1:
                        # Falls zu viele Informationen übergaben wurde, wird die Nachricht als ungültig klassifiziert
                        if len(wahlInfo) > 3:
                            self.fehlerhafteStimmen.append(wahlInfo)
                            return

                        # Falls die richtige Anzahl der Wajl parameter übergeben wurde
                        elif len(wahlInfo) == 3:
                            # Umwandeln in lower case, um fehler zu vermeiden
                            stimme = wahlInfo[2].lower()
                            for option in self.wahlAuswahl:
                                # Überprüft, ob abgegebene Stimme in den zur verfügungen stehenden Varianten ist
                                if stimme in self.wahlAuswahl[option] and len(self.wahlAuswahl[option]) > 1:
                                    # Funktion, die Alle Varianten einer Option auf nur eine Variante dieser Option Abbildet
                                    # Bsp: Max/Mustermann/MaxMustermann wird immer nur auf 'Max' abgebildet
                                    abbild = self.wahlAuswahl[option]
                                    # Erhöhung der 'Ja' Stimmen für diese Option um eins
                                    self.wahlurne[abbild[0]][0] += 1
                                    await massage.channel.send("Danke für deine Stimmabgabe!")
                                    return

                                # Abschnitt für nein-Stimmen
                                elif stimme == "nein":
                                    abbild = self.wahlAuswahl[option]
                                    self.wahlurne[abbild[0]][1] += 1
                                    await massage.channel.send("Danke für deine Stimmabgabe!")
                                    return
                                # Abschnitt für Enthaltungen
                                elif stimme == "enthaltung":
                                    abbild = self.wahlAuswahl[option]
                                    self.wahlurne[abbild[0]][2] += 1
                                    await massage.channel.send("Danke für deine Stimmabgabe!")
                                    return

                                # Falls keine Variante passt, ist es eine fehlerhafte/ungültige Stimme
                                else:
                                    self.fehlerhafteStimmen.append(stimme)
                                    await massage.channel.send("Danke für deine Stimmabgabe!")
                                    return

                    # Abschnitt der Stimmenabgabe, bei der mehrere Optionen zur Auswahl stehen
                    else:
                        # Falls zu viele Informationen übergeben wurden, wird die Stimme als fehlerhaft betrachtet
                        if len(wahlInfo) > 3:
                            self.fehlerhafteStimmen.append(wahlInfo)
                            return

                        # Falls die exakte Parameteranzahl übergeben wurde
                        elif len(wahlInfo) == 3:
                            # Umwandeln der Optionen in Lower Case
                            stimme = wahlInfo[2].lower()

                            for option in self.wahlAuswahl:
                                # Überprüft, ob die Abgegebene Stimme in den Optionen vorhanden ist
                                if stimme in self.wahlAuswahl[option] and len(self.wahlAuswahl[option]) > 1:
                                    # Bildet alle Varianten einer Option auf nur eine Variante dieser Option ab
                                    # Bsp: Max/Mustermann/MaxMustermann wird immer nur auf 'Max' abgebildet
                                    abbild = self.wahlAuswahl[option]
                                    # Erhöhung der 'Ja' stimmen der jeweiligen Option
                                    self.wahlurne[abbild[0]][0] += 1
                                    await massage.channel.send("Danke für deine Stimmabgabe!")
                                    return

                            # Erhöhung der 'Enthaltung' stimmen der jeweiligen Option
                            if stimme == "enthaltung":
                                abbild = self.wahlAuswahl["enthaltung"]
                                self.wahlurne[abbild[0]][0] += 1
                                await massage.channel.send("Danke für deine Stimmabgabe!")
                                return

                            # Erhöhung der 'Nein' stimmen der jeweiligen Option
                            elif stimme == "nein":
                                abbild = self.wahlAuswahl["nein"]
                                self.wahlurne[abbild[0]][0] += 1
                                await massage.channel.send("Danke für deine Stimmabgabe!")
                                return

                            # Falls keine Varante passt: Fehlerhafte Stimme
                            else:
                                self.fehlerhafteStimmen.append(stimme)
                                await massage.channel.send("Danke für deine Stimmabgabe!")
                                return



            # Falls Informationen bei $wahl fehlen meldet dies der Bot zurück und löscht den Bot Befehl
            else:
                await massage.channel.send("Ich habe dich leider nicht verstanden.")

                async for msg in massage.channel.history():
                    if str(msg.content).startswith("$wahl"):
                        await msg.delete()

                return

        # --- Abschnitt zur Initialisierung der Wahlliste in einem spezifischen Chanell ---
        if massage.content.startswith == "$init_liste":

            # Überprüft, welche Rollen ein User inne hat
            userRoles = []
            try:
                for role in massage.author.roles:
                    userRoles.append(str(role.name).lower())
            except:
                return

            # Funktion kann nur Verwendet werden, falls er*sie 'Redeleitung' ist
            if "redeleitung" in userRoles or "administrator" in userRoles:
                self.redelisteID = massage.channel.id
                await massage.channel.send("Channel wurde für Redeleitung initialisiert.")
                return

            # Falls nicht, passiert nichts
            else:
                return

        # ----- Abschnitt zur Redeliste -----
        # Überprft, ob die gesendete Channel ID mit der initialisierten Redelisten-Chanell-ID übereinstimmt
        if int(massage.channel.id) == self.redelisteID:

            # Überprüft, welche Rollen ein User inne hat
            userRoles = []
            try:
                for role in massage.author.roles:
                    userRoles.append(str(role.name).lower())
            except:
                pass

            # Abschnitt zum expliziten muten vin speziellen Usern, muss dafür 'Redeleitung' oder 'Administrator' sein
            if massage.content.startswith("$mute") and ("redeleitung" in userRoles or "administrator" in userRoles):
                # Befehl und zu mutendes Mitglied wird mit Leerzeichen getrennt $mute NAME
                msgInfo = str(massage.content).split(" ")
                # Member wird herausgesucht
                try:
                    memberToEdit = discord.utils.get(massage.guild.members, name=msgInfo[2])

                # Falls Member nicht gefunden werden konnte, passiert nichts
                except:
                    return

                # Falls member nicht gemutet ist, wird sie*er gemuted
                if msgInfo[1].lower() == "true" and len(msgInfo) == 3:
                    await memberToEdit.edit(mute=True)
                    return

                # Falls gemuted, wird es aufgehoben
                elif msgInfo[1].lower() == "false" and len(msgInfo) == 3:
                    await memberToEdit.edit(mute=False)
                    return

                # Falls etwas falsch angegeben wurde passiert nichts
                else:
                    await massage.channel.send("Ich habe dich leider nicht verstanden.")
                    return

            # ----- Abschnitt zur Redeliste -----
            if massage.content.startswith("$redeliste"):
                #Filter nur den Namen von der folgenden Nummer
                authorInfo = str(massage.author).split("#")
                # Speichert die ID des Autors
                authorID = massage.author.id
                # Trennt den weiterfolgenden Befehl ab
                msgInfo = str(massage.content).split(" ")
                # Speichert den Namen
                speakerName = authorInfo[0]

                # Löscht Bot-Befehl
                async for msg in massage.channel.history():
                    if str(msg.content).startswith("$redeliste"):
                        await msg.delete()

                # Zum manuellen Einfügen von Mitgliedern auf die Redeliste
                if len(msgInfo) == 3 and msgInfo[1] == "include":
                    includedUser = discord.utils.get(massage.guild.members, name=msgInfo[2])
                    self.direktDazu.insert(0, includedUser.id)
                    return

                # Falls zu wenig Infos übergebe wurden, wird dies mitgeteilt
                if len(msgInfo) < 2:
                    await massage.channel.send("{}, ich habe dich leider nicht verstanden...".format(str(speakerName)))
                    return

                # Abschnitt für normalen Redebeitrag
                if msgInfo[1] == "normal":
                    # Prüft, ob die Redeliste offen ist
                    if self.redeListeOffen:
                        # Überprüft, ob die aktuelle Redeliste nicht leer ist und ob die Person schon auf der Redeliste steht
                        if not (not self.redeListe) and authorID == self.redeListe[-1]:
                            await massage.channel.send(
                                "{}, du stehst bereits als Letztes auf der Liste...".format((str(speakerName))))
                            return

                        # Person wird auf die Redeliste gesetzt und die aktuelle Listenposition wird ausgegeben
                        else:
                            self.redeListe.append(authorID)
                            out = speakerName + " ist nun auf der Redeliste an Stelle {}!".format(len(self.redeListe))
                            await massage.channel.send(out)
                            return

                    else:
                        # Falls Liste geschlossen, wird die zurückgemeldet
                        await massage.channel.send(
                            "{}, leider ist die Redeliste schon geschlossen.".format(str(speakerName)))
                        return

                # Abschnitt für GO-Anträge, welche besonders hervorgehoben werden
                if msgInfo[1] == "go":
                    await massage.channel.send("------ \t \t ------ ")

                    await massage.channel.send("\t\t\t\t {} hat einen GO-Antrag".format(massage.author.name))

                    await massage.channel.send("------ \t \t ------ ")




                # Abschnitt für ein Direkt-Dazu
                elif msgInfo[1] == "direkt":

                    #Prüft, ob die Redeliste offen ist
                    if self.redeListeOffen:
                        # currentSpeaker = client.get_user(self.currentSpeaker) # Momentan nicht benutzt

                        # Prüft, ob Liste nicht-leer und ob die Person bereits auf der Liste steht
                        if not (not self.direktDazu) and authorID == self.direktDazu[-1]:
                            await massage.channel.send(
                                "{}, du stehst bereits als Letztes auf der Direkt-Dazu-Liste...".format(
                                    (str(speakerName))))
                            return

                        # Person wird auf die Lste gesetzt und Listenposition wird zurückgegeben
                        else:
                            self.direktDazu.append(authorID)
                            out = speakerName + " ist nun auf der Direkt-Dazu-Liste an Stelle {}!".format(
                                len(self.direktDazu))
                            await massage.channel.send(out)
                            return

                    # Falls Infos fehlen, wird dies zurückgemeldet und es passiert nichts
                    else:
                        await massage.channel.send(
                            "{}, leider ist die Redeliste schon geschlossen.".format(str(speakerName)))
                        return

                # Abschnitt zum Anzeigen der Redelsite
                elif msgInfo[1].lower() == "show":
                    #Prüft, ob Anfragesteller*in Redeleitung ist, falls nicht, passiert nichts
                    if "redeleitung" not in userRoles:
                        return

                    else:
                        # Falls zu wenig Infos übergeben wurden, wird dies zurückgegeben
                        if len(msgInfo) < 3:
                            await massage.channel.send(
                                "{}, ich habe dich leider nicht verstanden...".format(str(speakerName)))
                            return

                        else:
                            # Zeigt die normale Redeliste an
                            if msgInfo[2] == "normal":
                                redelisteAktuell = []
                                # Gespeicherte IDs werden in Namen umgewandelt und ausgegeben
                                for id in self.redeListe:
                                    redelisteAktuell.append(str(client.get_user(id).name))
                                await massage.channel.send(
                                    "Momentan stehen {} auf der Redeliste.".format(redelisteAktuell))
                                return

                            # Zeigt die 'Direkt-Dazu'-Liste an
                            elif msgInfo[2] == "direkt":
                                redelisteAktuell = []
                                for id in self.direktDazu:
                                    # Wandelt gespeicherte IDs in Namen um und gibt diese aus
                                    redelisteAktuell.append(str(client.get_user(id).name))
                                await massage.channel.send(
                                    "Momentan stehen {} auf der Direkt-Dazu-liste.".format(redelisteAktuell))
                                return

                            # Zeigt beide Listen an
                            elif msgInfo[2] == "all":
                                redelisteAktuell = []
                                for id in self.redeListe:
                                    redelisteAktuell.append(str(client.get_user(id).name))
                                await massage.channel.send(
                                    "Momentan stehen {} auf der Redeliste.".format(redelisteAktuell))

                                # Kurzes Warten, damit sich Prog nicht verschluckt
                                time.sleep(0.3)

                                direktDazuAktuell = []
                                for id in self.direktDazu:
                                    direktDazuAktuell.append(str(client.get_user(id).name))
                                await massage.channel.send(
                                    "Momentan stehen {} auf der Direkt-Dazu-liste.".format(direktDazuAktuell))
                                return

                            # Falls Infos fehlen, wird dies zurückgegeben
                            else:
                                await massage.channel.send(
                                    "{}, ich habe dich leider nicht verstanden...".format(str(speakerName)))
                                return



                # Abschnitt zum Löschen der Redeliste(n)
                elif msgInfo[1] == "delete":
                    # Prüft, ob Redeliste in den Rollen des Users ist
                    if "redeleitung" not in userRoles:
                        return

                    else:
                        # Falls zu wenig Parameter übergeben wurden
                        if len(msgInfo) < 3:
                            await massage.channel.send(
                                "{}, ich habe dich leider nicht verstanden...".format(str(speakerName)))
                            return

                        else:
                            # Setzt die normale Redeliste zurück
                            if msgInfo[2] == "normal":
                                self.redeListe = []
                                return

                            # Setzt die 'Direkt-Dazu' Redeliste zurück
                            elif msgInfo[2] == "direkt":
                                self.direktDazu = []
                                return

                            # Setzt bei Redelisten zurück
                            elif msgInfo[2] == "all":
                                self.redeListe = []
                                self.direktDazu = []
                                return

                            # Falls falsche Parameter übergeben wurden
                            else:
                                await massage.channel.send(
                                    "{}, ich habe dich leider nicht verstanden...".format(str(speakerName)))
                                return

                # Abschnitt zum Schließen der Redeliste
                elif msgInfo[1] == "close":
                    # Prüft, ob User nicht die Redeleitung inne hat
                    if "redeleitung" not in userRoles:
                        return

                    # Setzt den Parameter zum Öffnen der Redeliste auf False
                    else:
                        self.redeListeOffen = False
                        await massage.channel.send("Die Redeliste wurde soeben gechlossen.")
                        return

                # Abschnitt zum (Wieder-)Öffnen der Redeliste
                elif msgInfo[1] == "open":
                    # Prüft, ob User nicht die Redeleitung inne hat
                    if "redeleitung" not in userRoles:
                        return

                    # Setzt den Parameter zum Öffnen der Redeliste auf True
                    else:
                        self.redeListeOffen = True
                        await massage.channel.send("Die Redeliste wurde soeben geöffnet.")
                        return

                # Abschnitt zum Abarbeiten der Redeliste
                elif msgInfo[1] == "next":
                    # Prüft, ob User die Redeleitung nicht inne hat, falls nicht, passiert nichts
                    if "redeleitung" not in userRoles:
                        return

                    else:
                        # Falls Direkt-Dazu Liste leer ist
                        if not self.direktDazu:
                            # Falls die normale Redeliste auch leer ist, wird dies zurückgegeben
                            if not self.redeListe:
                                await massage.channel.send("Die Redeliste ist leer!")
                                # Setzt aktuelle Sprecher zurück
                                self.currentSpeaker = None
                                self.currentDirectSpeaker = None
                                if self.strictSpeechList:
                                    pass

                                return

                            # Falls Redeliste NICHT leer ist
                            else:
                                # Neue*r Sprecher*in wird aus der Redeliste genommen
                                # und als aktuelle*r Sprecher*in gesetzt
                                speaker = client.get_user(self.redeListe.pop(0))
                                self.currentSpeaker = speaker.id
                                self.currentDirectSpeaker = None
                                await massage.channel.send(
                                    "Als nächstes spricht: {}".format(str(speaker.name)))

                                return

                        # Falls die 'Direkt-Dazu'- Liste NICHT leer ist
                        else:
                            # Neue*r Sprecher*in wird aus der Redeliste genommen
                            # und als aktuelle*r Sprecher*in gesetzt
                            speaker = client.get_user(self.direktDazu.pop(0))
                            self.currentDirectSpeaker = speaker.id
                            await massage.channel.send(
                                "Als nächstes spricht direkt dazu: {}".format(str(speaker.name)))
                            return

                else:
                    await massage.channel.send("{}, ich habe dich leider nicht verstanden...".format(str(speakerName)))
                    return




            # Abschnitt für die Lösch-Funktion - nur für Admins
            elif massage.content.startswith("$clean"):
                # Prüft ob User die Rolle Administator inne hat
                if "administrator" not in userRoles:
                    return

                else:
                    # Art der Löschung wird bestimmt und vom vorigen Befehl mit Leerzeichen getrennt
                    cleanSort = str(massage.content).split(" ")
                    # Abschnitt zur Löschung aller Nachrichten in einem Textchat
                    if cleanSort[1] == "fullclean":
                        async for msg in massage.channel.history():
                            await msg.delete()
                        return

                    # Abschnitt für Löscung nur der Bot-Nachrichten
                    elif cleanSort[1] == "botmsg":
                        async for msg in massage.channel.history():
                            if str(msg.author.name).startswith(""): #TODO: HIER NAMEN EINFÜGEN
                                await msg.delete()

                        # Löscht den Bot Befehl
                        async for msg in massage.channel.history():
                            if str(msg.content).startswith("$clean"):
                                await msg.delete()

                        return

                    # Falls kein weiterer Parameter oder Flascher übergeben wurde, wird der Befehl gelöscht
                    # und nichts passiert
                    else:
                        async for msg in massage.channel.history():
                            if str(msg.content).startswith("$clean"):
                                await msg.delete()
                        return

            else:
                return


client = MyClient()
client.run("") #TODO: Hier Token einfügen
